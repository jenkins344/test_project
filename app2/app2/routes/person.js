const express=require('express')

const router=express.Router();
const cryptoJS=require('crypto-js')
const mysql=require('mysql2');
const db=require('../db')
const utils=require('../utils');
const { request } = require('http');
const { response } = require('express');

router.get('/',(request,response)=>{
    console.log("hi")
const query='select id,firstName,email,password from person';
db.pool.execute(query,(error,person)=>{
    response.send(utils.createResult(error,person))
})

router.post('/add',(request,response)=>{
    const {firstName,email,password}=request.body;
    const cryptedPassword=String(cryptoJS.MD5(password));
    const query='insert into person (firstName,email,password) values(?,?,?)';

    db.pool.execute(query,[firstName,email,password],(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.get('/get',(request,response)=>{
    const {id}=request.body;
    const query='select firstName,email,password from person where id=?';
    db.pool.execute(query,[id],(error,person)=>{
        if(person.length >0)
        {
            response.send(utils.createResult(error,person));
        }
        else{
            response.send("Person not found!!");
        }
        
    })
})

router.put('/update',(request,response)=>{
    const {id,firstName,email,password}=request.body;
    const query='update person set firstName=?,email=?,password=? where id=?';
    db.pool.execute(query,[firstName,email,password,id],(error,person)=>{
        response.send(utils.createResult(error,person));
    })
})

router.delete('/delete',(request,response)=>{
    const {id}=request.body;
    const query='delete from person where id=?'
    db.pool.execute(query,[id],(error,person)=>{
        response.send(utils.createResult(error,person));
    })
})

})
module.exports= router
